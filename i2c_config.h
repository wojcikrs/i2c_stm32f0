#ifndef I2C_CONFIG_H_
#define I2C_CONFIG_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>

/* Hardware abstract */
#include "device.h"
#include "rcc.h"
#include "nvic.h"

/* Exported types ------------------------------------------------------------*/
typedef struct
    {
        uint32_t timing;
        uint32_t filter_analog;
        uint32_t filter_digital;
        uint32_t mode;
        uint32_t own_address1;
        uint32_t ack;
        uint32_t acknowledged_address;
} config_t;

typedef struct
{
    const device_t *i2c;
    config_t config;
    uint8_t priority;
} i2c_config_t;

#endif /* I2C_CONFIG_H_ */
