/* Includes ------------------------------------------------------------------*/
#include "i2c.h"
#include "i2c_config.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>

/* Hardware abstract */
#include "device.h"

#include "rcc.h"
#include "nvic.h"

/* Exported constants --------------------------------------------------------*/
#define CR1_CLEAR_MASK                 ((uint32_t)0x00CFE0FF)
#define CR2_CLEAR_MASK                 ((uint32_t)0x07FF7FFF)
#define TIMING_CLEAR_MASK              ((uint32_t)0xF0FFFFFF)

#define I2C_CR1_PE                     ((uint32_t)0x00000001) // Peripheral enable */
#define I2C_CR1_TXIE                   ((uint32_t)0x00000002) // TX interrupt enable */
#define I2C_CR1_RXIE                   ((uint32_t)0x00000004) // RX interrupt enable */
#define I2C_CR1_ADDRIE                 ((uint32_t)0x00000008) // Address match interrupt enable */
#define I2C_CR1_NACKIE                 ((uint32_t)0x00000010) // NACK received interrupt enable */
#define I2C_CR1_STOPIE                 ((uint32_t)0x00000020) // STOP detection interrupt enable */
#define I2C_CR1_TCIE                   ((uint32_t)0x00000040) // Transfer complete interrupt enable */
#define I2C_CR1_ERRIE                  ((uint32_t)0x00000080) // Errors interrupt enable */
#define I2C_CR1_DFN                    ((uint32_t)0x00000F00) // Digital noise filter */
#define I2C_CR1_ANFOFF                 ((uint32_t)0x00001000) // Analog noise filter OFF */
#define I2C_CR1_SWRST                  ((uint32_t)0x00002000) // Software reset */
#define I2C_CR1_TXDMAEN                ((uint32_t)0x00004000) // DMA transmission requests enable */
#define I2C_CR1_RXDMAEN                ((uint32_t)0x00008000) // DMA reception requests enable */
#define I2C_CR1_SBC                    ((uint32_t)0x00010000) // Slave byte control */
#define I2C_CR1_NOSTRETCH              ((uint32_t)0x00020000) // Clock stretching disable */
#define I2C_CR1_WUPEN                  ((uint32_t)0x00040000) // Wakeup from STOP enable */
#define I2C_CR1_GCEN                   ((uint32_t)0x00080000) // General call enable */
#define I2C_CR1_SMBHEN                 ((uint32_t)0x00100000) // SMBus host address enable */
#define I2C_CR1_SMBDEN                 ((uint32_t)0x00200000) // SMBus device default address enable */
#define I2C_CR1_ALERTEN                ((uint32_t)0x00400000) // SMBus alert enable */
#define I2C_CR1_PECEN                  ((uint32_t)0x00800000) // PEC enable */
#define I2C_CR2_SADD                   ((uint32_t)0x000003FF) // Slave address (master mode) */
#define I2C_CR2_RD_WRN                 ((uint32_t)0x00000400) // Transfer direction (master mode) */
#define I2C_CR2_ADD10                  ((uint32_t)0x00000800) // 10-bit addressing mode (master mode) */
#define I2C_CR2_HEAD10R                ((uint32_t)0x00001000) // 10-bit address header only read direction (master mode) */
#define I2C_CR2_START                  ((uint32_t)0x00002000) // START generation */
#define I2C_CR2_STOP                   ((uint32_t)0x00004000) // STOP generation (master mode) */
#define I2C_CR2_NACK                   ((uint32_t)0x00008000) // NACK generation (slave mode) */
#define I2C_CR2_NBYTES                 ((uint32_t)0x00FF0000) // Number of bytes */
#define I2C_CR2_RELOAD                 ((uint32_t)0x01000000) // NBYTES reload mode */
#define I2C_CR2_AUTOEND                ((uint32_t)0x02000000) // Automatic end mode (master mode) */
#define I2C_CR2_PECBYTE                ((uint32_t)0x04000000) // Packet error checking byte */
#define I2C_OAR1_OA1                   ((uint32_t)0x000003FF) // Interface own address 1 */
#define I2C_OAR1_OA1MODE               ((uint32_t)0x00000400) // Own address 1 10-bit mode */
#define I2C_OAR1_OA1EN                 ((uint32_t)0x00008000) // Own address 1 enable */
#define I2C_OAR2_OA2                   ((uint32_t)0x000000FE) // Interface own address 2 */
#define I2C_OAR2_OA2MSK                ((uint32_t)0x00000700) // Own address 2 masks */
#define I2C_OAR2_OA2EN                 ((uint32_t)0x00008000) // Own address 2 enable */
#define I2C_TIMINGR_SCLL               ((uint32_t)0x000000FF) // SCL low period (master mode) */
#define I2C_TIMINGR_SCLH               ((uint32_t)0x0000FF00) // SCL high period (master mode) */
#define I2C_TIMINGR_SDADEL             ((uint32_t)0x000F0000) // Data hold time */
#define I2C_TIMINGR_SCLDEL             ((uint32_t)0x00F00000) // Data setup time */
#define I2C_TIMINGR_PRESC              ((uint32_t)0xF0000000) // Timings prescaler */
#define I2C_TIMEOUTR_TIMEOUTA          ((uint32_t)0x00000FFF) // Bus timeout A */
#define I2C_TIMEOUTR_TIDLE             ((uint32_t)0x00001000) // Idle clock timeout detection */
#define I2C_TIMEOUTR_TIMOUTEN          ((uint32_t)0x00008000) // Clock timeout enable */
#define I2C_TIMEOUTR_TIMEOUTB          ((uint32_t)0x0FFF0000) // Bus timeout B*/
#define I2C_TIMEOUTR_TEXTEN            ((uint32_t)0x80000000) // Extended clock timeout enable */
#define I2C_ISR_TXE                    ((uint32_t)0x00000001) // Transmit data register empty */
#define I2C_ISR_TXIS                   ((uint32_t)0x00000002) // Transmit interrupt status */
#define I2C_ISR_RXNE                   ((uint32_t)0x00000004) // Receive data register not empty */
#define I2C_ISR_ADDR                   ((uint32_t)0x00000008) // Address matched (slave mode)*/
#define I2C_ISR_NACKF                  ((uint32_t)0x00000010) // NACK received flag */
#define I2C_ISR_STOPF                  ((uint32_t)0x00000020) // STOP detection flag */
#define I2C_ISR_TC                     ((uint32_t)0x00000040) // Transfer complete (master mode) */
#define I2C_ISR_TCR                    ((uint32_t)0x00000080) // Transfer complete reload */
#define I2C_ISR_BERR                   ((uint32_t)0x00000100) // Bus error */
#define I2C_ISR_ARLO                   ((uint32_t)0x00000200) // Arbitration lost */
#define I2C_ISR_OVR                    ((uint32_t)0x00000400) // Overrun/Underrun */
#define I2C_ISR_PECERR                 ((uint32_t)0x00000800) // PEC error in reception */
#define I2C_ISR_TIMEOUT                ((uint32_t)0x00001000) // Timeout or Tlow detection flag */
#define I2C_ISR_ALERT                  ((uint32_t)0x00002000) // SMBus alert */
#define I2C_ISR_BUSY                   ((uint32_t)0x00008000) // Bus busy */
#define I2C_ISR_DIR                    ((uint32_t)0x00010000) // Transfer direction (slave mode) */
#define I2C_ISR_ADDCODE                ((uint32_t)0x00FE0000) // Address match code (slave mode) */
#define I2C_ICR_ADDRCF                 ((uint32_t)0x00000008) // Address matched clear flag */
#define I2C_ICR_NACKCF                 ((uint32_t)0x00000010) // NACK clear flag */
#define I2C_ICR_STOPCF                 ((uint32_t)0x00000020) // STOP detection clear flag */
#define I2C_ICR_BERRCF                 ((uint32_t)0x00000100) // Bus error clear flag */
#define I2C_ICR_ARLOCF                 ((uint32_t)0x00000200) // Arbitration lost clear flag */
#define I2C_ICR_OVRCF                  ((uint32_t)0x00000400) // Overrun/Underrun clear flag */
#define I2C_ICR_PECCF                  ((uint32_t)0x00000800) // PAC error clear flag */
#define I2C_ICR_TIMOUTCF               ((uint32_t)0x00001000) // Timeout clear flag */
#define I2C_ICR_ALERTCF                ((uint32_t)0x00002000) // Alert clear flag */
#define I2C_PECR_PEC                   ((uint32_t)0x000000FF) //PEC register */
#define I2C_RXDR_RXDATA                ((uint32_t)0x000000FF) // 8-bit receive data */
#define I2C_TXDR_TXDATA                ((uint32_t)0x000000FF) // 8-bit transmit data */
//
#define I2C(i2c)    ((i2c_t *)(i2c))

/* Private types ------------------------------------------------------------*/
typedef struct
{
    volatile uint32_t CR1;      // I2C Control register 1
    volatile uint32_t CR2;      // I2C Control register 2
    volatile uint32_t OAR1;     // I2C Own address 1 register
    volatile uint32_t OAR2;     // I2C Own address 2 register
    volatile uint32_t TIMINGR;  // I2C Timing register
    volatile uint32_t TIMEOUTR; // I2C Timeout register
    volatile uint32_t ISR;      // I2C Interrupt and status register
    volatile uint32_t ICR;      // I2C Interrupt clear register
    volatile uint32_t PECR;     // I2C PEC register
    volatile uint32_t RXDR;     // I2C Receive data register
    volatile uint32_t TXDR;     // I2C Transmit data register
} i2c_t;

typedef struct
{
    const device_t * i2c;

    uint8_t instance;
    int8_t error;

    /*RX TX */
    SemaphoreHandle_t rxtx_sem;
    SemaphoreHandle_t rxtx_mut;

    volatile uint8_t *rxtx_ptr;
    volatile uint32_t rxtx_cnt;
    uint32_t rxtx_len;

    int32_t rxtx_status;

    uint32_t rxtx_time_wait;
    uint32_t rxtx_time_out;
} i2c_control_t;

/* ------------------------------------------------------------------------- */
extern const i2c_config_t i2c_config[1];
static i2c_control_t i2c_control[1];

/* ------------------------------------------------------------------------- */
void i2c_irq_handler_master(void *args);

/* ------------------------------------------------------------------------- */
int32_t i2c_open(int32_t dev_nr, void *set, uint32_t request, ...)
{
    i2c_control_t * const ctrl = &i2c_control[dev_nr];
    const config_t * const config = &i2c_config[dev_nr].config;
    int32_t ret = I2C_STATUS_OK;

    /* --------------------------------------------------------------------- */
    ctrl->i2c = i2c_config[dev_nr].i2c;

    /* FRTOS --------------------------------------------------------------- */
    ctrl->rxtx_sem = xSemaphoreCreateBinary();
    if (!(ctrl->rxtx_sem))
    {
        ret = I2C_STATUS_ERROR_FRTOS;
        goto error;
    }

    xSemaphoreTake(ctrl->rxtx_sem, 0);

    ctrl->rxtx_mut = xSemaphoreCreateBinary();
    if (!(ctrl->rxtx_mut))
    {
        ret = I2C_STATUS_ERROR_FRTOS;
        goto error;
    }

    xSemaphoreGive(ctrl->rxtx_mut);

    ctrl->rxtx_time_wait = portMAX_DELAY;
    ctrl->rxtx_time_out = 1000;

    /* I2C ----------------------------------------------------------------- */
    rcc_clk_enable(ctrl->i2c);

    /* Disable I2Cx Peripheral */
    I2C(_DEVICE(ctrl->i2c))->CR1 &= (uint32_t) ~((uint32_t)I2C_CR1_PE);

    /* I2Cx FILTERS Configuration -------------------------------------------*/
    /* Clear I2Cx CR1 register */
    I2C(_DEVICE(ctrl->i2c))->CR1 &= CR1_CLEAR_MASK;

    /* Configure I2Cx: analog and digital filter */
    /* Set ANFOFF bit according to I2C_AnalogFilter value */
    /* Set DFN bits according to I2C_DigitalFilter value */
    I2C(_DEVICE(ctrl->i2c))->CR1 |= (uint32_t) config->filter_analog
            | (config->filter_digital << 8);

    /* I2Cx TIMING Configuration --------------------------------------------*/
    /* Configure I2Cx: Timing */
    /* Set TIMINGR bits according to I2C_Timing */
    /* Write to I2Cx TIMING */
    I2C(_DEVICE(ctrl->i2c))->TIMINGR = config->timing & TIMING_CLEAR_MASK;

    /*I2Cx OARX Configuration -----------------------------------------------*/
    /* Clear OAR1 and OAR2 register */
    I2C(_DEVICE(ctrl->i2c))->OAR1 = (uint32_t) 0x00UL;
    I2C(_DEVICE(ctrl->i2c))->OAR2 = (uint32_t) 0x00UL;
    /* Configure I2Cx: Own Address1 and acknowledged address */
    /* Set OA1MODE bit according to I2C_AcknowledgedAddress value */
    /* Set OA1 bits according to I2C_OwnAddress1 value */
    I2C(_DEVICE(ctrl->i2c))->OAR1 = (uint32_t)(
            (uint32_t) config->acknowledged_address
                    | (uint32_t) config->own_address1
                    | (uint32_t) I2C_OAR1_OA1EN);

    /* I2C MODE Configuration -----------------------------------------------*/
    /* Configure I2Cx: mode */
    /* Set SMBDEN and SMBHEN bits according to I2C_Mode value */
    I2C(_DEVICE(ctrl->i2c))->CR1 |= config->mode;

    /*I2Cx ACK Configuration ------------------------------------------------*/
    /* Clear I2Cx CR2 register */
    I2C(_DEVICE(ctrl->i2c))->CR2 &= CR2_CLEAR_MASK;

    /* Configure I2Cx: acknowledgement */
    /* Set NACK bit according to I2C_Ack value */
    I2C(_DEVICE(ctrl->i2c))->CR2 |= config->ack;

    /* NVIC -----------------------------------------------------------------*/
    nvic_set_handler(ctrl->i2c, i2c_irq_handler_master, ctrl);
    nvic_set_priority(ctrl->i2c, i2c_config[dev_nr].priority);

		/* Enable nvic irq */
    nvic_set_enable(ctrl->i2c);

		/* Enable i2c device */
    I2C(_DEVICE(ctrl->i2c))->CR1 |= I2C_CR1_PE;

    error: ;
    return ret;
}

/* ------------------------------------------------------------------------- */
void i2c_close(int32_t dev_nr, void *set)
{
}

/* ------------------------------------------------------------------------- */
int32_t i2c_write(int32_t dev_nr, void *set, void *data_ptr, uint32_t data_len)
{
    i2c_control_t * const ctrl = &i2c_control[dev_nr];
    uint16_t *address_slave = set;
    int32_t ret;

    if (*address_slave == 0x00U)
    {
        ret = I2C_STATUS_ERROR_SLAVE_ADRESS;
        goto error;
    }

    if (data_ptr == NULL)
    {
        ret = I2C_STATUS_ERROR_DATA_PTR;
        goto error;
    }

    if (data_len == 0x00U)
    {
        ret = I2C_STATUS_ERROR_DATA_SIZE;
        goto error;
    }

    if (xSemaphoreTake(ctrl->rxtx_mut, ctrl->rxtx_time_wait) == pdFALSE)
    {
        ret = I2C_STATUS_ERROR_BUSY;
        goto error;
    }

    ctrl->rxtx_ptr = data_ptr;
    ctrl->rxtx_cnt = 0x00;
    ctrl->rxtx_len = data_len;

    /*Wait for busy*/
    while (I2C(_DEVICE(ctrl->i2c))->ISR & I2C_ISR_BUSY)
    {
    }

    /* Interupt */
    I2C(_DEVICE(ctrl->i2c))->CR1 |= I2C_CR1_TXIE | I2C_CR1_TCIE;

    /* Transfer */
    I2C(_DEVICE(ctrl->i2c))->CR2 &= (uint32_t) 0xF8000000UL;
    I2C(_DEVICE(ctrl->i2c))->CR2 |= (uint32_t)(
            ((uint32_t) * address_slave & I2C_CR2_SADD)
                    | (((uint32_t) data_len << 16) & I2C_CR2_NBYTES));
    /* Start */
    I2C(_DEVICE(ctrl->i2c))->CR2 |= I2C_CR2_START;

    /* Wait for semaphore Task go sleep */
    if (xSemaphoreTake(ctrl->rxtx_sem, ctrl->rxtx_time_out ) == pdTRUE)
    {
        ret = ctrl->rxtx_cnt;
        xSemaphoreGive(ctrl->rxtx_mut);
    } else
    {
        ret = I2C_STATUS_ERROR_TIMEOUT;
        goto error;
    }

    error: ;
    return ret;
}

/* ------------------------------------------------------------------------- */
int32_t i2c_read(int32_t dev_nr, void *set, void *data_ptr, uint32_t data_len)
{
    i2c_control_t * const ctrl = &i2c_control[dev_nr];
    uint16_t *address_slave = set;
    int32_t ret;

    if (*address_slave == 0x00U)
    {
        ret = I2C_STATUS_ERROR_SLAVE_ADRESS;
        goto error;
    }

    if (data_ptr == NULL)
    {
        ret = I2C_STATUS_ERROR_DATA_PTR;
        goto error;
    }

    if (data_len == 0x00U)
    {
        ret = I2C_STATUS_ERROR_DATA_SIZE;
        goto error;
    }

    if (xSemaphoreTake(ctrl->rxtx_mut, ctrl->rxtx_time_wait) == pdFALSE)
    {
        ret = I2C_STATUS_ERROR_BUSY;
        goto error;
    }

    ctrl->rxtx_ptr = data_ptr;
    ctrl->rxtx_cnt = 0x00;
    ctrl->rxtx_len = data_len;

    /*Wait for busy*/
    while (I2C(_DEVICE(ctrl->i2c))->ISR & I2C_ISR_BUSY)
    {
    }

    /* Interupt */
    I2C(_DEVICE(ctrl->i2c))->CR1 |= I2C_CR1_RXIE | I2C_CR1_TCIE;

    /* Transfer */
    I2C(_DEVICE(ctrl->i2c))->CR2 &= (uint32_t) 0xF8000000UL;
    I2C(_DEVICE(ctrl->i2c))->CR2 |= (uint32_t)(
            ((uint32_t) * address_slave & I2C_CR2_SADD)
                    | (((uint32_t) data_len << 16) & I2C_CR2_NBYTES)
                    | I2C_CR2_RD_WRN);
    /* Start */
    I2C(_DEVICE(ctrl->i2c))->CR2 |= I2C_CR2_START;

    /* Wait for semaphore Task go sleep */
    if (xSemaphoreTake(ctrl->rxtx_sem, ctrl->rxtx_time_out ) == pdTRUE)
    {
        ret = ctrl->rxtx_cnt;
        xSemaphoreGive(ctrl->rxtx_mut);
    } else
    {
        ret = I2C_STATUS_ERROR_TIMEOUT;
        goto error;
    }

    error: ;
    return ret;
}

/* ------------------------------------------------------------------------- */
int32_t i2c_ioctl(int32_t dev_nr, void *set, uint32_t request, ...)
{

    return 0;
}

/* ------------------------------------------------------------------------- */
void i2c_irq_handler_master(void *args)
{
    i2c_control_t * const ctrl = args;
    BaseType_t task;

    uint32_t isr = I2C(_DEVICE(ctrl->i2c))->ISR;
    uint32_t it = I2C(_DEVICE(ctrl->i2c))->CR1;

    if ((isr & I2C_ISR_TXIS) && (it & I2C_CR1_TXIE))
    {
        I2C(_DEVICE(ctrl->i2c))->TXDR = *ctrl->rxtx_ptr++;
        ctrl->rxtx_cnt++;
    }

    if ((isr & I2C_ISR_RXNE) && (it & I2C_CR1_RXIE))
    {
        *ctrl->rxtx_ptr++ = I2C(_DEVICE(ctrl->i2c))->RXDR;
        ctrl->rxtx_cnt++;
    }

    if ((isr & I2C_ISR_TC) && (it & I2C_CR1_TCIE))
    {
        I2C(_DEVICE(ctrl->i2c))->CR2 |= I2C_CR2_STOP;

        I2C(_DEVICE(ctrl->i2c))->CR1 &= (uint32_t) ~(I2C_CR1_TXIE
                | I2C_CR1_RXIE | I2C_CR1_TCIE);

        xSemaphoreGiveFromISR(ctrl->rxtx_sem, &task);
        if (task)
        {
            taskYIELD();
        }
    }
}

