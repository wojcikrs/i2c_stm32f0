/* Includes ------------------------------------------------------------------*/
#include "i2c_config.h"
#include "device.h"

/* Private define ------------------------------------------------------------*/
#define I2C_AnalogFilter_Enable         ((uint32_t)0x00000000UL)
#define I2C_AnalogFilter_Disable        ((uint32_t)0x00001000UL)
#define IS_I2C_DIGITAL_FILTER(FILTER)   ((FILTER) <= 0x0000000FUL)
#define I2C_Mode_I2C                    ((uint32_t)0x00000000UL)
#define I2C_Ack_Enable                  ((uint32_t)0x00000000UL)
#define I2C_Ack_Disable                 ((uint32_t)0x00008000UL)
#define I2C_AcknowledgedAddress_7bit    ((uint32_t)0x00000000UL)
//#define I2C_AcknowledgedAddress_10bit   I2C_OAR1_OA1MODE //not suppoted

/* Private variables ---------------------------------------------------------*/
const i2c_config_t i2c_config[1] =
    {       _I2C1,                        //
            0x10420F13UL,                 //
            I2C_AnalogFilter_Enable,      //
            0x00UL,                       //
            0x00UL,                       //
            0x00UL,                       //
            I2C_Ack_Enable,               //
            I2C_AcknowledgedAddress_7bit, //
            0x80                          //
    };
