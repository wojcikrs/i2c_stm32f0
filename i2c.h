#ifndef I2C_H_
#define I2C_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>


/* Exported constants --------------------------------------------------------*/
#define I2C_STATUS_OK                             (0)

#define I2C_STATUS_ERROR_DATA_SIZE                (-1)
#define I2C_STATUS_ERROR_DATA_PTR                 (-2)
#define I2C_STATUS_ERROR_SLAVE_ADRESS             (-3)

#define I2C_STATUS_ERROR_TIMEOUT                  (-4)
#define I2C_STATUS_ERROR_BUSY                     (-5)

#define I2C_STATUS_ERROR_FRTOS                    (-6)

int32_t i2c_open(int32_t dev_nr, void *set, uint32_t request, ...);
void i2c_close(int32_t dev_nr, void *set);
int32_t i2c_write(int32_t dev_nr, void *set, void *data_ptr,
        uint32_t data_len);
int32_t i2c_read(int32_t dev_nr, void *set, void *data_ptr,
        uint32_t data_len);
int32_t i2c_ioctl(int32_t dev_nr, void *set, uint32_t request, ...);


#endif /* I2C_H_ */
